# ROCm-HGEMM
Optimized half precision gemm assembly kernels on AMD Vega GPU

## Introduction

Based on the knowledge of AMD ROCm and Vega architecture, we adapt the optimization strategies from [MaxAs](https://github.com/NervanaSystems/maxas) and [GCNGEMM](https://github.com/hyln9/GCNGEMM). We implement a version hgemm on AMD Vega10. This can achieve 97% efficiency of theoritical peak performance.

## Requirements

[AMD ROCm environment](https://github.com/RadeonOpenCompute/ROCm)

[PyOpenCL](https://github.com/pyopencl/pyopencl)

[numpy](http://www.numpy.org/)

## Usage

```bash
./run.sh
```

## Files

* run.sh: example shell script
* isa2hsaco.sh: shell script to cmpile assembly kernel.
* gemm.py: python script to run hgemm.
* kernel_rocm.pl: perl script to generate assembly kernel.
* kernels/hgemm_col_nt_64x64_8x8_rocm.pm: gemm kernel templeate.

## Help Information
```bash
usage: gemm.py [options] <kernel_path>

GEMM driver & benchmark

positional arguments:
  kernel                   path to kernel binary file

optional arguments:
  -h, --help               show this help message and exit
  -r N, --repeat N         number of repeats for kernel execution
  -tA TRANS, --transA TRANS
                           transpose op(A)
  -tB TRANS, --transB TRANS
                           transpose op(B)
  -m SIZE                  matrix dimension m
  -n SIZE                  matrix dimension n
  -k SIZE                  matrix dimension k
  -a ALPHA, --alpha ALPHA  alpha
  -b BETA, --beta BETA     beta
  -t TYPE, --type TYPE     platform type(rocm, gcn)
  -c ARCH, --arch ARCH     machine arch(Fiji|Vega, gfx803|gfx900)
  -v, --verify             whether to do verify
```
